﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class Hound : MonoBehaviour
{
    private Manager manager;
    private NavMeshAgent agent;

    private GameObject target;
    private Animator anim;

    public float maxMoveSpeed = 20f;
    private float startMoveSpeed;
    public float moveSpeed = 1f;
    public float moveSpeedIncreaseRate = 0.01f;
    private float size;

    void Start()
    {
        Setup();
    }

    private void Setup()
    {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();

        agent = GetComponent<NavMeshAgent>();
        startMoveSpeed = moveSpeed;

        Vector3 scale = transform.localScale;
        size = scale.x;

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        ChaseTarget();
        Accelerate();
    }

    private void ChaseTarget()
    {
        try
        {
            agent.SetDestination(target.transform.position);
        }
        catch (Exception e) { }
    }

    private void Accelerate()
    {
        moveSpeed += moveSpeedIncreaseRate;
        agent.speed = moveSpeed;
    }

    public void ReduceMovespeed(float reduceBy)
    {
        moveSpeed -= reduceBy;
        if (moveSpeed < startMoveSpeed)
        {
            moveSpeed = startMoveSpeed;
        }
    }

    public void ChangeTarget(GameObject target)
    {
        this.target = target;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject player = other.gameObject;
            EatPlayer(player.GetComponent<Player>());
        }
    }

    private void EatPlayer(Player player)
    {
        player.Death();
        anim.SetTrigger("EatPlayer");
        Debug.Log("Nom Nom Nom. Yummy!");
        manager.RestartRound();
    }
}
