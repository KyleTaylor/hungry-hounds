﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveArrow : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        transform.SetParent(null);
    }

    public void TurnOffArrow()
    {
        gameObject.SetActive(false);
    }

    public void StartAnimation()
    {
        gameObject.SetActive(true);
    }
}
