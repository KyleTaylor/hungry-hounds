﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Camera cam;
    private Manager manager;
    //NavMesh
    private NavMeshAgent agent;
    //MoveArrow
    public GameObject moveToArrow;
    private Animator moveToAnim;

    public GameObject heldBallObject { get; private set; }
    public bool hasBall = false;
    public KeyCode passBallKey = KeyCode.Z;
    public bool canPassBall = false;
    //Canvas
    public Button tossBallButton;

    void Awake()
    {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        heldBallObject = transform.GetChild(0).gameObject;
        moveToAnim = moveToArrow.GetComponent<Animator>();
    }
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckForInputEvent();
    }

    private void CheckForInputEvent()
    {
        //Cancel casts & Channels
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ClearAllEvents();
            return;
        }
        //Right Click
        else if (Input.GetMouseButtonDown(1))
        {
            MoveTo();
        }
        //Z key for passing the ball
        else if (Input.GetKeyDown(passBallKey))
        {
            if (hasBall)
            {
                CanPassBall(true);
            }
        }
        //Left Click
        else if (Input.GetMouseButtonDown(0))
        {
            LeftClick();
        }
    }

    private void ClearAllEvents()
    {
        CanPassBall(false);
    }

    private void CanPassBall(bool canPass)
    {
        canPassBall = canPass;
        manager.TossBallButtonEffect(canPass);
    }

    private RaycastHit ClickRayCast()
    {
        //Raycast
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            return hit;
        }
        return hit;
    }

    private void MoveTo()
    {
        //Raycast
        RaycastHit hit = ClickRayCast();

        SpawnMoveToArrow(hit.point);
        agent.SetDestination(hit.point);
    }

    private void SpawnMoveToArrow(Vector3 spawnPos)
    {
        try
        {
            moveToArrow.SetActive(false);
        }
        catch (Exception e) { }
        moveToArrow.SetActive(true);
        moveToArrow.transform.position = spawnPos;
        //moveToAnim.SetTrigger("MoveArrow");
    }

    #region Ball
    private void LeftClick()
    {
        RaycastHit hit = ClickRayCast();

        //Can pass ball & clicks on player
        if (canPassBall)
        {
            GameObject target = hit.collider.gameObject;

            if (target.tag == "Player")
            {
                manager.AssignBallToPlayer(this.gameObject, target.gameObject);
                HeldBallStatus(false);
                canPassBall = false;
            }
        }
        //TODO Requires further development after server/client communication is finished
        else if (hit.collider.tag == "UI")
        {
            try
            {
                //If UI element click on is the tossBallButton and it is interactable
                if (hit.collider.GetComponent<Button>() == tossBallButton && tossBallButton.interactable)
                {
                    canPassBall = true;
                }
            }
            catch (Exception e) { }
        }
    }

    private void HeldBallStatus(bool enabled)
    {
        hasBall = enabled;
        heldBallObject.SetActive(enabled);

        tossBallButton.interactable = enabled;
    }

    public void ReceiveBall()
    {
        HeldBallStatus(true);
        ReceiveBallEffect();
    }

    private void ReceiveBallEffect()
    {
        manager.AssignNewTargetForHound(this.gameObject);
    }
    #endregion

    public void Death()
    {
        this.gameObject.SetActive(false);
    }


}
