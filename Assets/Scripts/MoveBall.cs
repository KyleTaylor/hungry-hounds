﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBall : MonoBehaviour
{
    public Manager manager;

    private Vector3 startPosition;

    //[HideInInspector]
    public GameObject target { private get; set; }
    private GameObject targetsBall;
    public float rateToMove = 0.5f;

    public float minDistance = 0.5f;

    public void ThrowBall(Vector3 startPosition, GameObject targetObject)
    {
        transform.position = startPosition;
        gameObject.SetActive(true);
        this.target = targetObject;
        this.startPosition = transform.position;
        targetsBall = targetObject.GetComponent<Player>().heldBallObject;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetsBall.transform.position, rateToMove*Time.deltaTime);
        CheckIfReachedPlayer();
    }

    private void CheckIfReachedPlayer()
    {
        if (Vector3.Distance(transform.position, targetsBall.transform.position) <= minDistance)
        {
            Debug.Log(target.name + " has De Ball");
            this.gameObject.SetActive(false);
            manager.PlayerRecievesBall(target.GetComponent<Player>());
        }
    }
}
