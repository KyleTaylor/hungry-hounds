﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Manager : MonoBehaviour
{
    //Server
    public int numberOfClients = 3;
    //Round Settings
    public float roundRestartTime = 3;

    //Player
    private GameObject[] playerObjects;
    public Transform[] playerSpawnPositions;
    public GameObject playerPrefab;
    //Hound
    public GameObject houndObject;
    private Hound hound;
    //Ball
    public GameObject ballObject;
    

    void Awake()
    {
        CreatePlayersForClients();
        ObjectSetup();
    }

    void Start()
    {      
        AssignBallToRandomPlayer();
    }

    private void GetSpawnPositions()
    {
        foreach (Transform child in transform)
        {
            if (child.tag == "SpawnPositions")
                for (int i = 0; i < child.childCount; i++)
                {
                    playerSpawnPositions.SetValue(child.GetChild(i), i);
                }
        }

    }

    private void CreatePlayersForClients()
    {
        GetSpawnPositions();
        //TODO Read number of clients and assign them a player
        for (int i = 0; i < numberOfClients; i++)
        {
            SpawnPlayer(playerSpawnPositions[i], i+1);
        }
    }

    private void SpawnPlayer(Transform transform, int number)
    {
        GameObject player = Instantiate(playerPrefab, transform.localPosition, new Quaternion());
        player.name += "_"+number.ToString();
    }

    private void ObjectSetup()
    {
        playerObjects = GameObject.FindGameObjectsWithTag("Player");
        hound = houndObject.GetComponent<Hound>();
    }

    public void RestartRound()
    {
        //AssignBallToRandomPlayer();
        //hound.ChangeTarget(null);
        StartCoroutine(RandomBallAssignDelay(roundRestartTime));
    }

    #region BallManagement
    IEnumerator RandomBallAssignDelay(float time)
    {
        //suspend execution for X seconds
        yield return new WaitForSeconds(time);
        AssignBallToRandomPlayer();
    }

    private void AssignBallToRandomPlayer()
    {
        int i = Random.Range(0, playerObjects.Length - 1);
        PlayerRecievesBall(playerObjects[i].GetComponent<Player>());
    }

    public void AssignBallToPlayer(GameObject oldPlayer, GameObject newPlayer)
    {
        //Switch on Ball and it moves from old player to new player
        Player oldPlayerScript = oldPlayer.GetComponent<Player>();
        
        //Disable ball above player's head
        oldPlayerScript.heldBallObject.SetActive(false);
        //Set moving ball's position
        ballObject.GetComponent<MoveBall>().ThrowBall(oldPlayerScript.heldBallObject.transform.position, newPlayer);

        //Assign Hound to follow ball
        AssignNewTargetForHound(ballObject);
    }

    public void PlayerRecievesBall(Player player)
    {
        player.ReceiveBall();
        AssignNewTargetForHound(player.gameObject);
    }

    #endregion

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignNewTargetForHound(GameObject player)
    {
        hound.ChangeTarget(player);
    }

    #region UI
    public void TossBallButtonEffect(bool enabled)
    {
        if (enabled)
        {

        }
    }
    #endregion

}
